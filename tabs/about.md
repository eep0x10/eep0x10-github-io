---
title: About
published: true
---
### 20 anos
### Estudante de Ciência da Computação 
## Interesse em: 
- Web Applications Pentest
- CTF's, HTB, THM
- Python
- Linux
- No tempo livre gosto de jogar games retro no raspberry (Retropie/Lakka), programar, jogar HTB e ouvir musica eletrônica.

Criei o blog com objetivo de documentar meu aprendizado na área de segurança, compartilhar minhas experiências e falar a respeito de assuntos que tenho interesse, então sinta-se livre para criticar, corrigir ou complementar meus posts, pois  meu maior foco é o aprendizado.

No momento sou iniciante na área de segurança, mas tenho muito interesse em me desenvolver e aprender mais a respeito.
