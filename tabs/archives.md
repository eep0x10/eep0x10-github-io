---
title: Notes
type: archives
published: true
tittle: Notes
---

# Labs / CTF

- [Hack the Box](https://www.hackthebox.eu/home)
- [Try Hack Me](https://www.tryhackme.com/)
- [Over the Wire](https://overthewire.org/)
- [Cybersec Labs](https://www.cyberseclabs.co.uk/)
- [OffSec](https://www.offensive-security.com/labs/)
- [Root me](https://www.root-me.org/)
- [PortSwigger](https://portswigger.net/web-security)
- [Hack Me](https://hack.me/s/)

- [Lab List OSCP](https://docs.google.com/spreadsheets/u/1/d/1dwSMIAPIam0PuRBkCiDI88pU3yzrqqHkDtBngUHNCw8/htmlview#)
